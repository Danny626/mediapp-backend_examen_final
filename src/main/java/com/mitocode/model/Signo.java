package com.mitocode.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name = "signo")
public class Signo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSigno;

	@ManyToOne
	@JoinColumn(name = "id_paciente", nullable = false)
	private Paciente paciente;
	
	//@PastOrPresent
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime fecha;
	
	//@Size(min = 1, max = 50, message = "Ingrese una temperatura correcta")
	@Min(value = 1, message = "Ingrese una temperatura correcta")
	@Max(value = 50, message = "Ingrese una temperatura correcta")
	@Column(name = "temperatura", nullable = true, columnDefinition = "Decimal(5,2)")
	private Double temperatura;
	
	@Min(value = 1, message = "Ingrese un pulso correcto")
	@Max(value = 199, message = "Ingrese un pulso correcto")
	@Column(name = "pulso", nullable = true, columnDefinition = "Decimal(5,2)")
	private Double pulso;
	
	@Min(value = 1, message = "Ingrese un ritmo correcto")
	@Max(value = 199, message = "Ingrese un ritmo correcto")
	@Column(name = "ritmo_respiratorio", nullable = true, columnDefinition = "Decimal(5,2)")
	private Double ritmoRespiratorio;

	public Signo() {
	}

	public Signo(int idSigno, Paciente paciente, LocalDateTime fecha, Double temperatura, Double pulso,
			Double ritmoRespiratorio) {
		this.idSigno = idSigno;
		this.paciente = paciente;
		this.fecha = fecha;
		this.temperatura = temperatura;
		this.pulso = pulso;
		this.ritmoRespiratorio = ritmoRespiratorio;
	}

	public int getIdSigno() {
		return idSigno;
	}

	public void setIdSigno(int idSigno) {
		this.idSigno = idSigno;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	public Double getPulso() {
		return pulso;
	}

	public void setPulso(Double pulso) {
		this.pulso = pulso;
	}

	public Double getRitmoRespiratorio() {
		return ritmoRespiratorio;
	}

	public void setRitmoRespiratorio(Double ritmoRespiratorio) {
		this.ritmoRespiratorio = ritmoRespiratorio;
	}
	
	

}
