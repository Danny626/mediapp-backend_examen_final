package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Signo;

@Repository
public interface ISignoDAO extends JpaRepository<Signo, Integer> {

}
